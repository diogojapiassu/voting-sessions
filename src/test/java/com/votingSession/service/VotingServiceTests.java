package com.votingSession.service;

import com.votingSession.repository.VotingRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class VotingServiceTests {

    private VotingService votingService;

    @MockBean
    private VotingRepository votingRepository;

    @Before
    public void setUp() {
        votingService = new VotingService(votingRepository);
        Mockito.when(votingRepository.countBySessionIdAndUserId(1L, 1L)).thenReturn(1L);
    }

    @Test(expected = RuntimeException.class)
    public void checkIfUserAlreadyVotedInTheSessionTest(){
        votingService.checkIfUserAlreadyVotedInTheSession(1L, 1L);
    }

}
