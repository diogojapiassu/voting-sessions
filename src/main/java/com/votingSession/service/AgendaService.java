package com.votingSession.service;


import com.votingSession.entity.Agenda;
import com.votingSession.repository.AgendaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
@Service
public class AgendaService {

    @Autowired
    private AgendaRepository agendaRepository;

    public Agenda save(Agenda agenda){
        return agendaRepository.save(agenda);
    }

    public Optional<Agenda> findById(Long id) {
        return agendaRepository.findById(id);
    }

}
