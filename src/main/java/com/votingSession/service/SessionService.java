package com.votingSession.service;


import com.votingSession.entity.Session;
import com.votingSession.repository.SessionRepository;
import jakarta.persistence.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class SessionService {

    @Autowired
    private SessionRepository sessionRepository;

    public Session save(Session session){
        session.setStartDateTime(LocalDateTime.now());
        return sessionRepository.save(session);
    }

    public Optional<Session> findById(Long id) {
        return sessionRepository.findById(id);
    }

    public void checkIfSessionExists(Long id){
        Optional<Session> session = sessionRepository.findById(id);
        if(session.isEmpty()){
            throw new EntityNotFoundException("No entry was found for" + " id: " + id);
        }
    }

    public void checkIfVotingSessionIsInProgress(Long id){
        if(isVotingSessionIsInProgress(id)){
            throw new RuntimeException("Voting session is still in progress.");
        }
    }

    public void checkIfVotingSessionIsFinished(Long id){
        if(!isVotingSessionIsInProgress(id)){
            throw new RuntimeException("Voting session period has expired.");
        }
    }

    public boolean isVotingSessionIsInProgress(Long id){
        Session session = findById(id).get();
        LocalDateTime expireDateTime = session.getStartDateTime().plusMinutes(session.getMinutesDuration());
        return LocalDateTime.now().isBefore(expireDateTime);
    }



}
