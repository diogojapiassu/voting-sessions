package com.votingSession.service;


import com.votingSession.entity.Voting;
import com.votingSession.repository.VotingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class VotingService {


    private VotingRepository votingRepository;

    @Autowired
    public VotingService(VotingRepository votingRepository) {
        this.votingRepository = votingRepository;
    }

    public Voting save(Voting voting){
        return votingRepository.save(voting);
    }

    public boolean getResultsFromVotingSession(Long sessionId){
        long votesForYes = votingRepository.countBySessionIdAndVote(sessionId, true);
        long votesForNo = votingRepository.countBySessionIdAndVote(sessionId, false);

        return votesForYes >= votesForNo;
    }

    public void checkIfUserAlreadyVotedInTheSession(Long sessionId, Long userId){
        long userVotesInTheSession = votingRepository.countBySessionIdAndUserId(sessionId, userId);

        if(userVotesInTheSession > 0){
            throw new RuntimeException("This user has already voted in this session.");
        }
    }

    public Optional<Voting> findById(Long id) {
        return votingRepository.findById(id);
    }

}
