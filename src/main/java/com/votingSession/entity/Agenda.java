package com.votingSession.entity;

import jakarta.persistence.*;
import org.openapitools.model.MeetingAgenda;

import java.io.Serializable;

@Entity
public class Agenda implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String description;

    public Agenda() {

    }

    public Agenda(Long id) {
        this.id = id;
    }

    public Agenda(String description) {
        this.description = description;
    }

    public Agenda(Long id, String description) {
        this.id = id;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }



    public void setDescription(String description) {
        this.description = description;
    }


    public MeetingAgenda toMeetingAgenda(){
        MeetingAgenda meetingAgenda = new MeetingAgenda();
        meetingAgenda.setId(id);
        meetingAgenda.setDescription(description);
        return meetingAgenda;
    }
}
