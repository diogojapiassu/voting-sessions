package com.votingSession.entity;

import jakarta.persistence.*;
import org.openapitools.model.UserVote;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Voting implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Session session;

    @ManyToOne
    private User user;

    private boolean vote;

    public Voting() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isVote() {
        return vote;
    }

    public void setVote(boolean vote) {
        this.vote = vote;
    }

    public UserVote toUserVote(){
        UserVote userVote = new UserVote();
        userVote.setId(id);
        userVote.setSessionId(session.getId());
        userVote.setUserId(user.getId());
        userVote.setVote(vote);
        return userVote;
    }
}
