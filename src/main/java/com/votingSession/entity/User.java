package com.votingSession.entity;

import jakarta.persistence.*;
import org.openapitools.model.UserApp;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "users")
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;
    private String firstName;
    private String lastName;
    private String email;
    private String phone;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public User() {
    }

    public User(Long id) {
        this.id = id;
    }

    public UserApp toUserApp(){
        UserApp userApp = new UserApp();
        userApp.setId(id);
        userApp.setUsername(username);
        userApp.setFirstName(firstName);
        userApp.setLastName(lastName);
        userApp.setEmail(email);
        userApp.setPhone(phone);
        return userApp;
    }
}
