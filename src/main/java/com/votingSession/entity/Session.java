package com.votingSession.entity;

import jakarta.persistence.*;
import org.openapitools.model.VotingSession;

import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
public class Session implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String description;

    @ManyToOne
    private Agenda agenda;

    private LocalDateTime startDateTime;

    private Long minutesDuration;

    public Session() {

    }

    public Session(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Agenda getAgenda() {
        return agenda;
    }

    public void setAgenda(Agenda agenda) {
        this.agenda = agenda;
    }

    public Long getMinutesDuration() {
        return minutesDuration;
    }

    public void setMinutesDuration(Long minutesDuration) {
        this.minutesDuration = minutesDuration;
    }


    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    public void setStartDateTime(LocalDateTime startDateTime) {
        this.startDateTime = startDateTime;
    }

    public VotingSession toVotingSession(){
        VotingSession votingSession = new VotingSession();
        votingSession.setId(id);
        votingSession.description(description);
        votingSession.agendaId(agenda.getId());
        votingSession.minutesDuration(minutesDuration);
        return votingSession;
    }
}
