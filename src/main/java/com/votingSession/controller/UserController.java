package com.votingSession.controller;


import com.votingSession.entity.User;
import com.votingSession.service.UserService;
import org.openapitools.api.UserApi;
import org.openapitools.model.UserApp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class UserController implements UserApi {

    @Autowired
    public UserService userService;

    @Override
    public ResponseEntity<UserApp> createUser(UserApp userApp) {
        User user = new User();
        user.setUsername(userApp.getUsername());
        user.setFirstName(userApp.getFirstName());
        user.setLastName(userApp.getLastName());
        user.setEmail(userApp.getEmail());
        user.setPhone(userApp.getPhone());

        User savedUser = userService.save(user);

        return new ResponseEntity<>(savedUser.toUserApp(), HttpStatus.CREATED);
    }
}
