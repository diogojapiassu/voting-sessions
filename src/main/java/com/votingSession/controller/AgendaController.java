package com.votingSession.controller;

import com.votingSession.entity.Agenda;
import com.votingSession.service.AgendaService;
import org.openapitools.api.AgendaApi;
import org.openapitools.model.MeetingAgenda;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class AgendaController implements AgendaApi {

    private static final Logger logger = LoggerFactory.getLogger(AgendaController.class);

    @Autowired
    public AgendaService agendaService;


    @Override
    public ResponseEntity getAgendaById(Long agendaId) {
        logger.debug("AgendaController -> getAgendaById");
        Agenda agenda = agendaService.findById(agendaId).get();
        return new ResponseEntity<>(agenda, HttpStatus.OK);
    }

    @Override
    public ResponseEntity<MeetingAgenda> addAgenda(MeetingAgenda meetingAgenda) {
        logger.debug("AgendaController -> addAgenda");
        Agenda agenda = new Agenda(meetingAgenda.getDescription());
        Agenda savedAgenda = agendaService.save(agenda);
        return new ResponseEntity<>(savedAgenda.toMeetingAgenda(), HttpStatus.CREATED);
    }
}
