package com.votingSession.controller;

import com.votingSession.entity.Agenda;
import com.votingSession.entity.Session;
import com.votingSession.service.SessionService;
import com.votingSession.service.VotingService;
import jakarta.persistence.EntityNotFoundException;
import org.openapitools.api.SessionApi;
import org.openapitools.model.VotingResult;
import org.openapitools.model.VotingSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class SessionController implements SessionApi {

    @Autowired
    public SessionService sessionService;

    @Autowired
    public VotingService votingService;

    @Override
    public ResponseEntity<VotingSession> addVotingSession(VotingSession votingSession) {
        Session session = new Session();
        session.setDescription(votingSession.getDescription());
        session.setAgenda(new Agenda(votingSession.getAgendaId()));
        if(votingSession.getMinutesDuration() == null || votingSession.getMinutesDuration() == 0L){
            session.setMinutesDuration(1L);
        }else{
            session.setMinutesDuration(votingSession.getMinutesDuration());
        }

        Session savedSession = sessionService.save(session);
        return new ResponseEntity<>(savedSession.toVotingSession(), HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity getResultSession(Long sessionId) {
        try {
            sessionService.checkIfSessionExists(sessionId);
            sessionService.checkIfVotingSessionIsInProgress(sessionId);
            VotingResult votingResult = new VotingResult();
            votingResult.setResult(votingService.getResultsFromVotingSession(sessionId));
            return new ResponseEntity<>(votingResult, HttpStatus.OK);
        } catch (EntityNotFoundException e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
        }catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }

    }
}
