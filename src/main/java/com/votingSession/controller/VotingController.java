package com.votingSession.controller;

import com.votingSession.entity.Session;
import com.votingSession.entity.User;
import com.votingSession.entity.Voting;
import com.votingSession.service.SessionService;
import com.votingSession.service.VotingService;
import org.openapitools.api.VotingApi;
import org.openapitools.model.UserVote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class VotingController implements VotingApi {

    @Autowired
    public VotingService votingService;

    @Autowired
    public SessionService sessionService;

    @Override
    public ResponseEntity addVote(UserVote userVote) {
        try {
            sessionService.checkIfVotingSessionIsFinished(userVote.getSessionId());
            votingService.checkIfUserAlreadyVotedInTheSession(userVote.getSessionId(), userVote.getUserId());

            Voting voting = new Voting();
            voting.setSession(new Session(userVote.getSessionId()));
            voting.setUser(new User(userVote.getUserId()));
            voting.setVote(userVote.getVote());

            Voting savedVoting = votingService.save(voting);
            return new ResponseEntity<>(savedVoting.toUserVote(), HttpStatus.CREATED);
        } catch (Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        }
    }
}
