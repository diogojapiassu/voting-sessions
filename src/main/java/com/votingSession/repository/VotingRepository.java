package com.votingSession.repository;


import com.votingSession.entity.Voting;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface VotingRepository extends JpaRepository<Voting, Long> {

    List<Voting> findBySessionId(Long sessionId);

    long countBySessionIdAndVote(Long sessionId, boolean vote);

    long countBySessionIdAndUserId(Long sessionId, Long userId);

}
