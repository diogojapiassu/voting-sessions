# Voting Sessions

## What I need to install on my machine?

- Docker (recommended version:  20.10.13, build a224086)
- docker-compose (recommended version:  1.29.2, build 5becea4c)

## How to start the Voting Sessions application?

### Run the following commands below:

1. mvn clean package
2. docker-compose up (if there are any changes in app, please use docker-compose up --build)

## How to execute and test the APIs?

Please, import the API specification. 
Recommended frameworks:
- Swagger Editor
- Postman
- Stoplight

File location: src/main/resources/static/api-spec/votingSessions-v1.yaml

# Any questions?
Diogo Japiassu \
diogojapiassu@gmail.com \
+55 (62) 99913-6073 